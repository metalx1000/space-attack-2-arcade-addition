Document: space-attack-2
Title: Debian space-attack-2 Manual
Author: <insert document author here>
Abstract: This manual describes what space-attack-2 is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/space-attack-2/space-attack-2.sgml.gz

Format: postscript
Files: /usr/share/doc/space-attack-2/space-attack-2.ps.gz

Format: text
Files: /usr/share/doc/space-attack-2/space-attack-2.text.gz

Format: HTML
Index: /usr/share/doc/space-attack-2/html/index.html
Files: /usr/share/doc/space-attack-2/html/*.html
