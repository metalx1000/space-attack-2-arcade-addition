project=space-attack-2
DIR=build/bin
file=$(DIR)/$(project)

$(shell mkdir -p $(DIR))

$(project): $(file).pck

all: $(file).apk $(file).html $(file).exe

$(file).pck:
	godot3-server --path "src" --export-pack "pck" "../$(file).pck"
	cp README.md $(DIR)/
	cp LICENSE $(DIR)/
	@echo "pack file is built in $(file).pck"

$(file).apk:
	godot3-server --path "src" --export-debug "Android" "../$(file).apk"
	@echo "apk file is built in $(file).apk"
	find build/bin

$(file).html:
	godot3-server --path "src" --export-debug "HTML5" "../$(file).html"

$(file).exe:
	godot3-server --path "src" --export-debug "Windows" "../$(file).exe"

pck: $(file).pck
apk: $(file).apk
html: $(file).html
exe: $(file).exe

clean:
	$(info removing build folder)
	rm build -fr
