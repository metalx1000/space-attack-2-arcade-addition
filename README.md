Space Attack 2
==============

Sequel to Space Attack using the Godot Engine

<a href="https://filmsbykris.com/games/2021/Space-Attack-2">https://filmsbykris.com/games/2021/Space-Attack-2</a> 

Download the 
<a href="https://gitlab.com/metalx1000/my-games/-/tree/master/space-attack_2">binaries here</a>

Get it in the <a href="https://play.google.com/store/apps/details?id=com.filmsbykris.spaceattack2">Google Play Store</a>

# Screen Shots
<img src="screenshots/20201210104474.png" width="128">
<img src="screenshots/20201210104475.png" width="128">
<img src="screenshots/20201210104470.png" width="128">
<img src="screenshots/20201210104458.png" width="128">
<img src="screenshots/20201210104459.png" width="128">
<img src="screenshots/20201210104460.png" width="128">
<img src="screenshots/20201210104457.png" width="128">
<img src="screenshots/20201210104472.png" width="128">
<img src="screenshots/20201210104471.png" width="128">

Story
=====

The Cube of Meta-Tron's location has been discovered and the race is on for all who desire is't powers.  The 5 Solids of the Platonic symbolizing the power of it's geometric pattern is found through the universe.  But, the central essence of it power is found in the small cube that has been missing for centuries.  Divine is it's energy as it flows through creation and connects to every electron in the Universe as it provides a path for communications and power.  A power that is uncontainable outside of the cube.  As races from all planets converge on the lost relic it's becoming clear that not all intent is honorable or good.  Some seek it's power to dominate and control, while others seek it out for good, and yet other search for it just to make sure it's power stays unused.  The Universe wants this power to be discovered and for it to be used for good.  All you know is that you have been hired to get it first.  Swarms of enemy ships converge upon your location.  Path's of vessels intercept your advancement towards The Cube of Meta-Tron!!!

Game Play
=========

<a href="https://lbry.tv/@metalx1000:1/Space-Attack-2---Meta-Trons-Cube:6">Video</a>

Credits
=======
# Game Design and Programming
* [Kris Occhipinti] (http://filmsbykris.com)

# Spaceship Art by: Skorpio
* http://opengameart.org/content/space-ship-construction-kit
* Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)- https://creativecommons.org/licenses/by-sa/3.0/


# Music [By teknoaxe] (http://teknoaxe.com/)
* [Can't Go Backwards - Chiptune/8-bit] (https://www.youtube.com/watch?v=rSZzP_twLaY)
* [Not According to Plan] (https://www.youtube.com/watch?v=bsb-hS3A378)
* [Beyond the Fear] (https://www.youtube.com/watch?v=lMEA9IbhJh0)
* [Anthem at the Tables] (https://www.youtube.com/watch?v=7EjCKT-O9d8)
* [Electron - Breakbeat_Synthwave] (https://www.youtube.com/watch?v=yibESRDRb2Y)
* [Out Glitch the Glitch] (https://www.youtube.com/watch?v=mkzv73LZS8A)
* [Reprogrammed Reality] (https://www.youtube.com/watch?v=CFwpB2UILZI)
* [Memory Allocation] (https://www.youtube.com/watch?v=QnKrHms7ZJ4)

# Sounds
* [jalastram] (http://opengameart.org/content/8-bit-explosions-free-sound-effects-1)
* [Spaceship Art by: Skorpi] (http://opengameart.org/content/space-ship-construction-kit)
* [Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0] (https://creativecommons.org/licenses/by-sa/3.0/)


License
=======
* License: GPLv3 (see [LICENSE](https://gitlab.com/metalx1000/space-attack-2/-/blob/master/LICENSE) for more information)
