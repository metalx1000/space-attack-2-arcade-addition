extends KinematicBody2D

var plLaser = preload("res://Lasers_Cube.tscn")
var plExplosion = preload("res://Explosions.tscn")
var plCubeBomb = preload("res://cube_bomb.tscn")

var life = 1000
var speed = 1500
var berserk_speed = 1
var stop_distance = 128
var move = true

onready var pos = Vector2(get_viewport_rect().size.x/2,get_viewport_rect().size.y/3)

# Called when the node enters the scene tree for the first time.
func _ready():
	Main.load_music("res://res/music/cube.ogg")
	Main.enemies_active = false
	position.x = get_viewport_rect().size.x/2
	position.y = -256
	$Sprite.play("default")

func _process(delta):
	if life < 1:
		death()
	else:
		Main.boss_life = life
		
	if move:
		move_to(delta)

func move_to(delta):
	var move_pos = pos - position
	if position.distance_to(pos) > stop_distance:
		move_pos = move_pos.normalized()
		move_and_slide(move_pos * speed * berserk_speed * delta * 4)
	else:
		move = false
		new_action()
		
func create_laser(lspeed,dir,snd):
	var laser = plLaser.instance()
	var _laser = laser.get_node("laser")
	_laser.snd = snd
	_laser.add_to_group("enemy_laser")
	laser.modulate.r = 1
	laser.modulate.g = 0
	laser.modulate.b = 0
	laser.modulate.a = 1
	_laser.speed = lspeed
	_laser.dir = dir
	_laser.pos = position
	get_tree().current_scene.add_child(laser)

func random_pos():
	berserk_speed = rand_range(2,6)
	var x = rand_range(0,get_viewport_rect().size.x)
	var y = rand_range(0,get_viewport_rect().size.y)
	pos = Vector2(x,y)
	move = true

func random_attack():
	var attack = floor(rand_range(0,3))
	if attack == 1:
		attack_1()
	elif attack == 2:
		bombs()

	
func attack_1():
	for i in range(0,150,5):
		var ii = i * .1
		create_laser(500,ii,true)
		yield(get_tree().create_timer(.1), "timeout")
		
func bombs():
	for i in range(0,3):
		var bomb = plCubeBomb.instance()
		bomb.position = position
		get_tree().current_scene.add_child(bomb)
		yield(get_tree().create_timer(.2), "timeout")
		random_pos()

func new_action():
	var action = rand_range(0,2)
	if action > 1:
		$Sprite.play("spin2")
		yield(get_tree().create_timer(2), "timeout")
		$Sprite.play("spin")
		yield(get_tree().create_timer(2), "timeout")
		random_attack()
		random_pos()
	else:
		random_pos()

func death():
	Main.boss_life = 0
	Main.change_music("res://res/music/final_music.ogg")
	var explosion = plExplosion.instance()
	explosion.position = position
	get_tree().current_scene.add_child(explosion)
	explosion.get_node("Explosion").play()
	Main.fade_scene = true
	queue_free()

func _on_Area2D_body_entered(body):
	if body.is_in_group("player_laser"):
		Main.score += 10
		var laser = body.get_parent()
		if laser.life == 0:
			laser.death()
		life -= laser.damage

		
	elif body.is_in_group("Players") and body.DEAD == false:
		body.death()
