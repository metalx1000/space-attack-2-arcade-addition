extends KinematicBody2D


var damage = 1
var life = 0
var stream_player = AudioStreamPlayer.new()
var snd = true

var speed = 750
var velocity = Vector2()
var pos = Vector2(200,300)
var dir = 0

#bullet direction
func _ready():
	start(pos,dir)
	if snd:
		play_sound()

func start(pos, dir):
	rotation = dir
	position = pos
	velocity = Vector2(speed, 0).rotated(rotation)

func play_sound():
#yield(get_tree().create_timer(1), "timeout")
	stream_player.stream = Main.laser_snd
	stream_player.stream.set_loop(false)
	
	stream_player.connect("finished", stream_player, "queue_free")
	add_child(stream_player)
	stream_player.play()

func _physics_process(delta):
	var collision = move_and_collide(velocity * delta)
	if collision:
		velocity = velocity.bounce(collision.normal)
		if collision.collider.has_method("hit"):
			collision.collider.hit()

func death():
	queue_free()

func _on_Life_Timer_timeout():
	death()
	pass # Replace with function body.
