extends "res://ship.gd"

func _ready():
	add_to_group("Players")
	add_to_group("Tagalong")
	#print(get_tree().get_nodes_in_group("Tagalong").size())
	explosion_scale = .5
	DEAD = false
	invincible = false
	speed = rand_range(20,200)
	stop_distance = rand_range(0,32)
	berserk_attack = 0
	berserk_speed = 0
	berserk_pos = Vector2(300,300)
	screen_size 
	type = "tagalong"
	inv = 3
	position.y = get_viewport_rect().size.y + 128
	position.x = rand_range(0,get_viewport_rect().size.x)
#var weapon = "default"

func death():
		yield(get_tree().create_timer(rand_range(.2,.8)),"timeout")
		$death_snd.play()
		visible = false
		DEAD = true
		$Death_Timer.start()
		explode()
		queue_free()
	
func update_position():
	pass

func _on_Area2D_body_entered(body):
	if body.is_in_group("enemy_laser"):
		body.get_parent().death()
		death()
