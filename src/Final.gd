extends Node2D

var message_pos = Vector2(10,0)
# Called when the node enters the scene tree for the first time.
func _ready():
	message_pos.y = get_viewport_rect().size.y
	var a = 0.0
	while a < 1:
		modulate.a = a
		a+=.01
		yield(get_tree().create_timer(.01),"timeout")
		
	Main.kills = 0
	Main.deaths = 0
	Main.score = 0
	Main.level = 1
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	message_pos.y -= 40 * delta
	$Label.set_position(message_pos)
	
	if $Label.margin_bottom < -128:
		get_tree().change_scene("res://Title_Screen.tscn")
