extends Sprite

var plLaser = preload("res://Lasers.tscn")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	#move off screen
	position.y = get_viewport_rect().size.y + 500
	pass

func attack():
	$activate.current_animation = "mother_ship"
	#$activate.playback_speed = 2
	#$activate.play()
	for i in 25:
		yield(get_tree().create_timer(rand_range(0,.5)), "timeout")
		if position.y < get_viewport_rect().size.y:
			create_laser()



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func create_laser():
	var scale = 6
	var laser = plLaser.instance()
	laser.get_node("laser").add_to_group("player_laser")
	var sound = laser.stream_player
	sound.set_pitch_scale(.25)
	sound.set_volume_db(10)
	laser.life = 1
	laser.damage = 3
	laser.modulate.r = 0
	laser.modulate.g = 0
	laser.modulate.b = 1
	laser.scale = Vector2(scale,scale)
	laser.position = position
	Main.color_flash(.02,1,Color(0,0,.5,1.0))
	get_tree().current_scene.add_child(laser)
