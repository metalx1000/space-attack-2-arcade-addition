extends Node2D
var tagalong = preload("res://Tagalong.tscn")
var wingman = preload("res://Wingman.tscn")
var cross_attack = preload("res://Cross-Attack.tscn")

onready var player = get_tree().get_root().get_node("World/Player")

var speed_x = 0
var speed_y = 0
var type = "none"

# Called when the node enters the scene tree for the first time.
func _ready():
	var items = $Sprite.frames.get_animation_names()
	var num = items.size()
	var i = floor(rand_range(0,num))
	$Sprite.play(items[i])
	type = items[i]
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):

	position.x += speed_x
	position.y += speed_y
	

func create_cross_attack():
	var ships = cross_attack.instance()
	get_tree().current_scene.add_child(ships)


func create_wingman():
	var ship = wingman.instance()
	#var sprite = ship.get_node("KinematicBody2D/Sprite")
	get_tree().current_scene.add_child(ship)

func create_tagalong():
	var ship = tagalong.instance()
	var sprite = ship.get_node("KinematicBody2D/Sprite")
	get_tree().current_scene.add_child(ship)

func _on_Area2D_body_entered(body):
	if body.name == "Player" and body.DEAD == false:
		Main.score += 20
		if type == "invincibility":
			body.set_invicibility(10)
		elif type == "rapid":
			Main.rapid_fire += 400
			Main.HUD("rapid",.7)
			
		elif type == "berserk":
			Main.color_flash(.02,50,Color(.7,0,0,1.0))
			Main.rapid_fire += 400
			body.berserk_attack = 200
			Main.HUD("berserk",.6)
		elif type == "plus":
			Main.plus = true
		elif type == "mega":
			Main.color_flash(.02,10,Color(.5,.5,.5,1.0))
			Main.HUD("mega",.7)
			Main.mega_det()
		elif type == "mother":
			Main.HUD("mother",.7)
			var ship = get_tree().get_nodes_in_group("mother_ship")[0]
			ship.attack()
		elif type == "tagalong":
			Main.HUD("tagalong",.7)
			create_tagalong()
		elif type == "wingman":
			Main.HUD("wingman",.7)
			create_wingman()
		elif type == "escape":
			Main.HUD("escape",.7)
			player.quick_escape()
		elif type == "cross_attack":
			Main.HUD("cross_attack",.7)
			create_cross_attack()
		else:
			Main.powerup = type
	
		queue_free()
