extends Node2D

var message_pos = Vector2(10,0)
var speed_up = 1
# Called when the node enters the scene tree for the first time.
func _ready():
	message_pos.y = get_viewport_rect().size.y
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	message_pos.y -= 40 * delta * speed_up
	$Label.set_position(message_pos)
	
	if $Label.margin_bottom < -128:
		get_tree().change_scene("res://Title_Screen.tscn")
		
func _input(event):
	#get_tree().change_scene("res://Title_Screen.tscn")
	if Input.is_action_just_pressed("ui_shoot"):
		speed_up = 4
	else:
		speed_up = 1


func _on_exit_button_up():
	get_tree().change_scene("res://Title_Screen.tscn")
