extends "res://ship.gd"


func _ready():
	add_to_group("Players")
	explosion_scale = 2
	invincible_able = true
	DEAD = false
	invincible = false
	speed = 500
	stop_distance = 5
	berserk_attack = 0
	berserk_speed = 0
	berserk_pos = Vector2(300,300)
	screen_size 
	type = "player"

	inv = 3
#var weapon = "default"

func update_position():
	Main.player_position = position

func _on_Area2D_body_entered(body):
	if body.is_in_group("enemy_laser"):
		body.get_parent().death()
		death()


func _on_shoot_power_timeout():
	Main.shoot_power += 1
