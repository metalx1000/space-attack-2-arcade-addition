extends Area2D

export var id = 0
var clicks = 0

func _ready():
	$sprite.play("default")

func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton \
	and event.button_index == BUTTON_LEFT \
	and event.is_pressed():
		clicks += 1
		if clicks >= 3:
			if id == 0:
				var new_target = get_tree().get_nodes_in_group("targets")[1]
				new_target.visible = true
			queue_free()
